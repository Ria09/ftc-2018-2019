package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@TeleOp(name="Test", group="Linear Opmode")
public class Test extends LinearOpMode {

    public Robot robot;

    public void runOpMode() throws InterruptedException {

        robot = new Robot(this);

        waitForStart();

        robot.init();

        while(opModeIsActive()) {
            robot.update();
        }

    }

}