package org.firstinspires.ftc.teamcode;

//import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.vuforia.CameraDevice;

import java.util.List;
//import java.util.concurrent.TimeUnit;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;


@Autonomous(name = "AutoSquare", group = "Linear OpMode")
public class AutoSquare extends LinearOpMode {
    public Robot robot;

    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    private static final String VUFORIA_KEY = "AfPfJhX/////AAABmQofITvlXEPpgKWnxHUWlTlbvDvhARJWuas96HLeiHoYPhCRIP3/rolh4MJDuv484dt9Wmi5MBJpdlgy4gJ4JhIEs3H6VygkkCmNxm9yS1+gerjR0Y029dsMeEF66fG7vmMtMTc9UkgKw7WAV6mdl6xZJh/HgqhP54zHqL5j6Vb4zRw7QlfxozPKU6s1H5hWtWBhwSCofsHPFg+DcLchOQ15gc1UiV9CrkbcRcX2pQXfrTq4+FSlc/kmI6h8XjYKg8uYomc/xuMitbSIMGCg0efwyZhHNCy+X2PJA322rgabcQCRuzbqMZOOJlgtCZeOJwdZq0r3ekydnTAtzDZry6C2uPXRbiI8aGmiomskazzc";

    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;

    @Override
    public void runOpMode() {
        robot = new Robot(this);

        // The TFObjectDetector uses the camera frames from the VuforiaLocalizer, so we create that
        // first.
        initVuforia();
        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        } else {
            telemetry.addData("Sorry!", "This device is not compatible with TFOD");
        }

        telemetry.update();

        CameraDevice.getInstance().setFlashTorchMode(true);

        waitForStart();

        robot.init();

        // robot.land();

        /* Activate Tensor Flow Object Detection. */
        if (tfod != null) {
            tfod.activate();
        }

        robot.toggleCubeServo();
        robot.autoMove(1, 0, 0);
        try {
            Thread.sleep(250);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        robot.autoMove(0, 0, 0);

        double oldTime = System.currentTimeMillis();
        int cycles = 1;
        boolean leftOrRight = false;

        while (opModeIsActive()) {
            if (tfod != null) {
                // getUpdatedRecognitions() will return null if no new information is available since
                // the last time that call was made.
                List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                int goldMineralXLeft = -1, goldMineralXRight = -1, goldMineralYBottom = -1;
                if (updatedRecognitions != null) {
                    for (Recognition recognition : updatedRecognitions) {
                        if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                            goldMineralXLeft = (int) recognition.getLeft();
                            goldMineralXRight = (int) recognition.getRight();
                            goldMineralYBottom = (int) recognition.getBottom();
                        }
                    }
                }
                telemetry.addData("# Object Detected", updatedRecognitions != null ? updatedRecognitions.size() : "null");
                if (goldMineralXLeft != -1) {
                    telemetry.addData("Gold Mineral Coordonate", goldMineralXLeft);
                    if ((double) (goldMineralXLeft + goldMineralXRight) / 2 <= ((double) 1280) / 100 * 40)
                        robot.autoMove(0, 0, (float) -0.25);
                    else if ((double) (goldMineralXLeft + goldMineralXRight) / 2 >= ((double) 1280) / 100 * 60)
                        robot.autoMove(0, 0, (float) 0.25);
                    else {
                        robot.autoMove((float) 0.5, 0, 0);
                        if (goldMineralYBottom >= 600) {

                            robot.autoMove(1, 0, 0);

                            try {
                                Thread.sleep(650);
                            } catch (InterruptedException ex) {
                                Thread.currentThread().interrupt();
                            }

                            robot.toggleCubeServo();
                            robot.autoMove(0, 0, 0);
                            break;
                        }
                    }
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    robot.autoMove(0, 0, 0);

                } else if (System.currentTimeMillis() - oldTime > 1000) {
                    telemetry.addData("cycles before: ", cycles);

                    if (cycles > 2) {
                        leftOrRight = !leftOrRight;
                        cycles = 0;
                    }
                    if (leftOrRight == true) {
                        robot.autoMove(0, 0, (float) -0.25);
                        ++cycles;
                    } else if (leftOrRight == false) {
                        robot.autoMove(0, 0, (float) 0.25);
                        ++cycles;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }

                    telemetry.addData("cycles after: ", cycles);
                    robot.autoMove(0, 0, 0);
                    oldTime = System.currentTimeMillis();
                }
                telemetry.update();
            }
        }

        if (tfod != null) {
            tfod.shutdown();
        }

    }

    /**
     * Initialize the Vuforia localization engine.
     */
    private void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraDirection = CameraDirection.BACK;

        //  Instantiate the Vuforia engine
        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        // Loading trackables is not necessary for the Tensor Flow Object Detection engine.
    }

    /**
     * Initialize the Tensor Flow Object Detection engine.
     */
    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfodParameters.minimumConfidence = 0.50;
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }
}