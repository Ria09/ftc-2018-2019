package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.lynx.LynxI2cColorRangeSensor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.CRServoImplEx;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.GyroSensor;
//import com.qualcomm.robotcore.hardware.Gyroscope;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

//import org.firstinspires.ftc.robotcontroller.external.samples.SensorREVColorDistance;
import org.firstinspires.ftc.robotcore.external.Telemetry;
//import org.firstinspires.ftc.robotcore.external.android.AndroidGyroscope;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

//import java.util.IllegalFormatCodePointException;

import Engine.Vector2;

public class Movement {

    LinearOpMode opMode;
    Telemetry telemetry;

    final DcMotor frontLeft;
    final DcMotor frontRight;
    final DcMotor backLeft;
    final DcMotor backRight;

    final DcMotor elevator;

    final DcMotor armAngleMotor;
    final DcMotor armLengthMotor;
    final CRServo armServo1;
    final CRServo armServo2;

    final Servo cubeServo;
    double cubeServoValue = 0.5;

    final double FAST = 1;
    final double SLOW = 0.45;
    final Gamepad driveGamepad;
    final Gamepad armGamepad;



    public Movement(LinearOpMode opMode) {
        this.opMode = opMode;
        this.telemetry = opMode.telemetry;
        frontLeft = opMode.hardwareMap.get(DcMotor.class, "front_left");
        frontRight = opMode.hardwareMap.get(DcMotor.class, "front_right");
        backLeft = opMode.hardwareMap.get(DcMotor.class, "back_left");
        backRight = opMode.hardwareMap.get(DcMotor.class, "back_right");

        elevator = opMode.hardwareMap.get(DcMotor.class, "elevator");

        cubeServo = opMode.hardwareMap.get(Servo.class, "cube_servo");

        armAngleMotor = opMode.hardwareMap.get(DcMotor.class, "arm_angle_motor");
        armLengthMotor = opMode.hardwareMap.get(DcMotor.class, "arm_length_motor");
        armServo1 = opMode.hardwareMap.get(CRServo.class, "arm_servo_1");
        armServo2 = opMode.hardwareMap.get(CRServo.class, "arm_servo_2");

        driveGamepad = opMode.gamepad1;
        armGamepad = opMode.gamepad2;
    }

    public void init() {

        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontLeft.setDirection(DcMotorSimple.Direction.REVERSE);
        backLeft.setDirection(DcMotorSimple.Direction.REVERSE);

        armAngleMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armLengthMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        armLengthMotor.setDirection(DcMotorSimple.Direction.REVERSE);

        cubeServo.setPosition(cubeServoValue);
    }

    public void manualCheck() {

        // drive Part
        // robot drive and elevator
        double speed = SLOW;

        if (driveGamepad.a) speed = FAST;
        else speed = SLOW;

        if (driveGamepad.y)
            elevator.setPower(1);
        else if (driveGamepad.b)
            elevator.setPower(-1);
        else elevator.setPower(0);


        double rotation = (driveGamepad.right_trigger - driveGamepad.left_trigger);
        Vector2 p = DirectionToDCMotorsPower(new Vector2(driveGamepad.left_stick_x, -driveGamepad.left_stick_y));
        // telemetry.addData("driveGamepad.left_stick_x:", driveGamepad.left_stick_x);
        // telemetry.addData("driveGamepad.left_stick_y:", driveGamepad.left_stick_y);
        double fl1 = p.getX() + rotation;
        double fr1 = p.getY() - rotation;
        double bl1 = p.getY() + rotation;
        double br1 = p.getX() - rotation;


        rotation = armGamepad.right_stick_x;
        p = DirectionToDCMotorsPower(new Vector2(armGamepad.left_stick_x, -armGamepad.left_stick_y));
        // telemetry.addData("armGamepad.left_stick_x:", armGamepad.left_stick_x);
        // telemetry.addData("armGamepad.left_stick_y:", armGamepad.left_stick_y);
        double fl2 = p.getX() + rotation;
        double fr2 = p.getY() - rotation;
        double bl2 = p.getY() + rotation;
        double br2 = p.getX() - rotation;
        if (fl1 == 0 && fr1 == 0 && bl1 == 0 && br1 == 0) {
            telemetry.addData("first if", "yeet");
            frontLeft.setPower(Range.clip(fl2, -1, 1) * speed);
            frontRight.setPower(Range.clip(fr2, -1, 1) * speed);
            backLeft.setPower(Range.clip(bl2, -1, 1) * speed);
            backRight.setPower(Range.clip(br2, -1, 1) * speed);
        } else {
            telemetry.addData("second if", "neet");
            frontLeft.setPower(Range.clip(fl1, -1, 1) * speed);
            frontRight.setPower(Range.clip(fr1, -1, 1) * speed);
            backLeft.setPower(Range.clip(bl1, -1, 1) * speed);
            backRight.setPower(Range.clip(br1, -1, 1) * speed);
        }
        // arm Part
        // arm controls
        armLengthMotor.setPower(armGamepad.right_stick_y * 0.25);
        armAngleMotor.setPower((armGamepad.right_trigger - armGamepad.left_trigger) * 0.50);
        if (armGamepad.left_bumper == true && armGamepad.right_bumper== false) {
            telemetry.addData("entered bumper", "left");
            armServo1.setPower(1);
            armServo2.setPower(-1);
        } else if (armGamepad.right_bumper == true && armGamepad.left_bumper == false) {
            telemetry.addData("entered bumper", "right");
            armServo1.setPower(-1);
            armServo2.setPower(1);
        }
        else {
            armServo1.setPower(0);
            armServo2.setPower(0);
        }

        if(armGamepad.y== true){
            toggleCubeServo();try {
                Thread.sleep(250);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

        telemetry.addData("Front Left Position: ", frontLeft.getCurrentPosition());
        telemetry.addData("Front Right Position: ", frontRight.getCurrentPosition());
        telemetry.addData("Back Left Position: ", backLeft.getCurrentPosition());
        telemetry.addData("Back Right Position: ", backRight.getCurrentPosition());
        telemetry.addData("Elevator Position: ", elevator.getCurrentPosition());
    }

    public void autoMove(float x, float y, float rotation) {

        Vector2 p = DirectionToDCMotorsPower(new Vector2(-y, x));

        double fl = p.getX() + rotation;
        double fr = p.getY() - rotation;
        double bl = p.getY() + rotation;
        double br = p.getX() - rotation;

        frontLeft.setPower(Range.clip(fl, -1, 1));
        frontRight.setPower(Range.clip(fr, -1, 1));
        backLeft.setPower(Range.clip(bl, -1, 1));
        backRight.setPower(Range.clip(br, -1, 1));

        telemetry.addData("Front Left Position: ", frontLeft.getCurrentPosition());
        telemetry.addData("Front Right Position: ", frontRight.getCurrentPosition());
        telemetry.addData("Back Left Position: ", backLeft.getCurrentPosition());
        telemetry.addData("Back Right Position: ", backRight.getCurrentPosition());
        telemetry.update();
    }

    public void toggleCubeServo() {
        if (cubeServoValue == 0.1)
            cubeServoValue = 0.5;
        else if (cubeServoValue == 0.5)
            cubeServoValue = 0.1;
        cubeServo.setPosition(cubeServoValue);
    }

    public void land() {

        while (!(elevator.getCurrentPosition()<=-31100 && elevator.getCurrentPosition()>=-31400))
            elevator.setPower(-1);
        elevator.setPower(0);
        autoMove(0, (float) -0.25, 0);
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void rotateToAngle0(Robot robot) {

        if (robot.getAngle()>180 && robot.getAngle()<=360) {
            while (!(robot.getAngle() >= 350 || robot.getAngle() <= 10)) {
                telemetry.addData("gyro:", robot.getAngle());
                telemetry.update();
                autoMove(0, 0, (float) -0.25);
            }
        } else if(robot.getAngle()>=0 || robot.getAngle()<=180){
            while (!(robot.getAngle() >= 350 || robot.getAngle()<=10)) {
                telemetry.addData("gyro:", robot.getAngle());
                telemetry.update();
                autoMove(0, 0, (float) 0.25);
            }
        }
        autoMove(0, 0, 0);
    }

    public Vector2 DirectionToDCMotorsPower(Vector2 direction) throws ArithmeticException {

        float angle = (float) Math.atan2(direction.getY(), direction.getX()) * 57.2957795f; // Angle in Deg
        angle -= 45; // Rotate with 45 deg
        angle *= 0.0174532925f;

        return new Vector2((float) Math.cos((double) angle) * direction.length(),
                (float) Math.sin((double) angle) * direction.length());
    }
}